/* eslint-disable no-case-declarations */
require('dotenv').config();
const _         = require('lodash');
const axios     = require('axios');
const Discord   = require('discord.js');
const mongoose  = require('mongoose');
const Jimp      = require('jimp');
const imgur     = require('imgur');

function log(message) {
  console.log(`\x1b[95m${process.env.NAME} //\x1b[0m ${message}`);
}

const client  = new Discord.Client();
const mongoDB     = `mongodb://${process.env.DB_HOST}/${process.env.DB_NAME}`;
mongoose.set('useFindAndModify', false);
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', () => {
  log('Mongoose connected to DB');
});

// const newHero = new Hero({
//   name: 'Abaddon',
//   aliases: ['abaddon'],
//   image_url: 'https://gamepedia.cursecdn.com/dota2_gamepedia/thumb/2/26/Abaddon_icon.png/150px-Abaddon_icon.png',
//   tier: 0,
//   assignee: null,
// });

// newHero.save().then(() => { console.log(`Stored to DB '${newHero.name}'`); });

client.login(process.env.DISCORD_TOKEN);

client.on('ready', () => {
  log('Discord client connected');
  client.user.setActivity('Server Requests', { type: 'LISTENING' })
    .then(presence => log(`Activity set to ${presence.game ? presence.game.name : 'none'}`))
    .catch(console.error);
});

client.on('message', (message) => {
  if (message.author.bot) return;
  if (!message.content.startsWith(process.env.PREFIX)) return;
  const request = message.content.slice(process.env.PREFIX.length, message.content.length);
  const command = request.split(' ')[0];
  const parameters = request.slice(command.length).trim().split(',');

  switch (command) {
    case 'help':
    case 'h':
      message.channel.send({
        embed: {
          title: `:speech_balloon: ${process.env.NAME} Commands`,
          color: process.env.DECIMAL_COLOUR,
          description: `
          **${process.env.PREFIX}invite**
          **${process.env.PREFIX}weather** <City>, <Country Code>
          **${process.env.PREFIX}champ**
          **${process.env.PREFIX}movie**
          `,
        },
      });
      break;
    case 'invite':
    case 'i':
      message.channel.send(`<https://discordapp.com/oauth2/authorize?client_id=${process.env.DISCORD_CLIENT_ID}&scope=bot&permissions=3072>`);
      break;
    case 'champ':
      const role = message.guild.roles.find(val => val.name === 'Discord UFC Champion');
      const champ = role.members.first();

      const imgTitle = Jimp.read('https://i.imgur.com/FUMfuIe.png');
      const imgAvi = Jimp.read(champ.user.avatarURL);
      const imgMask = Jimp.read('https://i.imgur.com/fnkQ6oi.png');

      Promise.all([imgTitle, imgAvi, imgMask]).then((images) => {
        const iTitle = images[0];
        const iAvi = images[1];
        const iMask = images[2];
        iTitle.composite(
          iAvi.resize(1000, 1000).mask(iMask, 0, 0), 500, 166.5,
        )
          .getBase64(Jimp.MIME_PNG, (err, src) => {
            imgur.uploadBase64(src.substring(22))
              .then((json) => {
                message.channel.send({
                  embed: new Discord.RichEmbed()
                    .setDescription(`${champ.displayName} is your reigning Discord UFC Champion!`)
                    .setImage(json.data.link)
                    .setColor('#F1C40F'),
                });
              })
              .catch((error) => {
                console.error(error.message);
              });
          });
      });
      break;
    case 'movie':
    case 'm':
      // let data = {
      //   "Title": "The Last Dance",
      //   "Year": "2020",
      //   "Rated": "TV-MA",
      //   "Released": "19 Apr 2020",
      //   "Runtime": "491 min",
      //   "Genre": "Documentary, Biography, History, Sport",
      //   "Director": "N/A",
      //   "Writer": "N/A",
      //   "Actors": "Phil Jackson, Michael Jordan, David Aldridge, Scottie Pippen",
      //   "Plot": "Charting the rise of the 1990's Chicago Bulls, led by Michael Jordan, one of the most notable dynasties in sports history.",
      //   "Language": "English",
      //   "Country": "USA",
      //   "Awards": "Won 1 Primetime Emmy. Another 4 wins & 2 nominations.",
      //   "Poster": "https://m.media-amazon.com/images/M/MV5BY2U1ZTU4OWItNGU2MC00MTg1LTk4NzUtYTk3ODhjYjI0MzlmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg",
      //   "Ratings": [
      //       {
      //           "Source": "Internet Movie Database",
      //           "Value": "9.2/10"
      //       }
      //   ],
      //   "Metascore": "N/A",
      //   "imdbRating": "9.2",
      //   "imdbVotes": "69,380",
      //   "imdbID": "tt8420184",
      //   "Type": "series",
      //   "totalSeasons": "1",
      //   "Response": "True"
      // }

      axios.get(`http://www.omdbapi.com/?apikey=${process.env.OMDB_API_KEY}&t=${parameters.join(' ').split('<')[0]}`)
        .then((response) => {
          const data = response.data;
          if (data.Response === 'True') {
            const msgEmbed = new Discord.RichEmbed({
              color: process.env.DECIMAL_COLOUR,
              thumbnail: { url: data.Poster },
              author: {
                name: `${data.Title} (${data.Year})`,
                icon_url: 'https://m.media-amazon.com/images/G/01/imdb/images-ANDW73HA/favicon_desktop_32x32.png',
              },
              description: `**[${data.Rated}]** ${data.Plot}`,
              fields: [
                {
                  name: 'IMDB Rating',
                  value: `:star: ${data.imdbRating}`,
                  inline: true,
                },
                {
                  name: 'Genre',
                  value: data.Genre,
                  inline: true,
                },
                {
                  name: 'Runtime',
                  value: data.Runtime,
                  inline: true,
                },
                {
                  name: 'Cast',
                  value: data.Actors,
                  inline: false,
                },
              ],
            });
            axios.get(`https://apicloud-colortag.p.rapidapi.com/tag-url.json?palette=precise&sort=relevance&url=${data.Poster}`, {
              headers: { 'X-RapidAPI-Key': `${process.env.COLORTAG_API_KEY}` },
            }).then((colourRes) => {
              msgEmbed.setColor(colourRes.data.tags[0].color);
            }).catch(() => {
            }).then(() => {
              message.channel.send(msgEmbed);
            });
          } else {
            message.channel.send(new Discord.RichEmbed({
              color: process.env.DECIMAL_COLOUR,
              author: {
                name: data.Error,
                icon_url: 'https://m.media-amazon.com/images/G/01/imdb/images-ANDW73HA/favicon_desktop_32x32.png',
              },
            }));
          }
        })
        .catch(() => {
        });
      break;
    case 'weather':
    case 'w':
      axios.get('http://api.openweathermap.org/data/2.5/weather', {
        params: {
          q: `${parameters[0]}${parameters[1] ? `,${parameters[1]}` : ''}`,
          appid: process.env.OPENWEATHERMAP_API_KEY,
        },
      })
        .then((response) => {
          const data = response.data;
          const msgEmbed = new Discord.RichEmbed({
            color: process.env.DECIMAL_COLOUR,
            thumbnail: { url: `http://openweathermap.org/img/w/${data.weather[0].icon}.png` },
            author: {
              name: `${data.name}, ${data.sys.country}`,
              icon_url: `https://www.countryflags.io/${data.sys.country.toLowerCase()}/flat/64.png`,
            },
            description: '',
            fields: [{
              name: 'Weather',
              value: _.startCase(data.weather[0].description),
              inline: true,
            },
            {
              name: 'Temperature',
              value: `${(data.main.temp - 273.15).toFixed(2)}°C / ${((data.main.temp - 273.15) * 9 / 5 + 32).toFixed(2)}°F`,
              inline: true,
            },
            {
              name: 'Cloudiness',
              value: `${data.clouds.all.toFixed(0)}%`,
              inline: true,
            },
            {
              name: 'Wind',
              value: `${data.wind.speed.toFixed(2)}m/s at ${data.wind.deg.toFixed(0)}°`,
              inline: true,
            },
            {
              name: 'Humidity',
              value: `${data.main.humidity.toFixed(0)}%`,
              inline: true,
            },
            {
              name: 'Pressure',
              value: `${data.main.pressure.toFixed(2)} hPa`,
              inline: true,
            },
            ],
          });
          axios.get(`https://apicloud-colortag.p.rapidapi.com/tag-url.json?palette=precise&sort=relevance&url=https://www.countryflags.io/${data.sys.country.toLowerCase()}/flat/64.png`, {
            headers: { 'X-RapidAPI-Key': `${process.env.COLORTAG_API_KEY}` },
          }).then((colourRes) => {
            msgEmbed.setColor(colourRes.data.tags[0].color);
          }).catch(() => {
          }).then(() => {
            message.channel.send(msgEmbed);
          });
        })
        .catch(() => {
        });

      break;

    default:
      break;
  }
});
